# Build and Run

```
podman build -t localhost/headscale .

podman run --rm -d --name headscale  \
    -p 8080:8080 -p 8443:8443        \
    -v headscale:/var/lib/headscale/ \
    localhost/headscale:latest

podman exec -it headscale bash
```

# Create a user for ACLs

```
headscale users create dustymabe
```

# On the machine that shares access to local home LAN:

```
sudo tailscale up --accept-dns=false --advertise-routes=192.168.100.0/24 --stateful-filtering=false --login-server=https://headscale.dustymabe.com:8443
```

NOTE: `--stateful-filtering=false` is needed since the behavior was changed for https://tailscale.com/security-bulletins#ts-2024-005

# Enable the route sharing

```
headscale routes enable -r 1
```


# On the machines that roam:
```
sudo tailscale up --accept-dns=false  --accept-routes --login-server=https://headscale.dustymabe.com:8443
```

Follow prompts to run a command on the headscale server to add the node.


# Ignore for now, was for running headscale on apu2 but currently it is running on dustymabe.com

```
#sudo firewall-cmd --add-port=8080/tcp --zone=isp
#sudo firewall-cmd --add-forward-port=port=80:proto=tcp:toport=7080 --zone=isp
```
